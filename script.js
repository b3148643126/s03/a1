// Define a grades property in our student class that will accept an array of 4 numbers ranging from 0 to 100 to be its Value. If the argument used does not satisfy all the conditions, this property will be set to undefined instead

class Student {
  constructor(name, email, grades) {
    this.name = name;
    this.email = email;
    if (
      Array.isArray(grades) &&
      grades.length === 4 &&
      grades.every(
        (grade) => typeof grade === "number" && grade >= 0 && grade <= 100
      )
    ) {
      this.grades = grades;
    } else {
      this.grades = undefined;
    }
  }

  login() {
    console.log(`${this.email} has logged in`);
    return this;
  }

  logout() {
    console.log(`${this.name} has logged off`);
    return this;
  }

  listGrades() {
    console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    return this;
  }

  computedAve() {
    let sum = 0;
    this.grades.forEach((grade) => (sum += grade));
    this.gradeAve = sum / 4;
    return this;
  }

  willPass() {
    this.passed = this.computeAve().gradeAve >= 85;
    return this;
  }
  willPassWithHonors() {
    if (this.passed) {
      this.passedWithHonors = this.gradeAve >= 90;
    } else {
      this.passedWithHonors = false;
    }
    return this;
  }
}

let studentOne = new Student("John", "john@mail.com", [89, 84, 78, 88]);
let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85]);
let studentThree = new Student("Jane", "jane@mail.com", [87, 89, 91, 93]);
let studentFour = new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93]);

// 1) What is the blueprint where objects are created from?
// Answer: class
// 2) What is the naming convention applied to classes?
// ANswer: pascal case
// 3) What keyword do we use to create objects from a class? answer: new
// 4) What is the technical term for creating an object from a class? answer: instantiation
// 5) What class method dictates HOW objects will be created from that class? answer: constructor
